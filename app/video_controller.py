from .video import Video


class VideoController:

    @staticmethod
    def add(**attributes):
        Video.add(**attributes)

    @staticmethod
    def update(**attributes):
        _id = attributes['id']
        del attributes['id']
        Video.update(_id, **attributes)

    @staticmethod
    def delete(**attributes):
        _id = attributes['id']
        Video.delete(_id)

    @staticmethod
    def get_all():
        return Video.get_all()

    @staticmethod
    def get_by_id(_id):
        return Video.get_by_id(_id)
