from app import db


class Model(db.Model):
    __abstract__ = True

    @staticmethod
    def commit_changes():
        db.session.commit()

    @classmethod
    def get_all(cls):
        data = cls.query.all()
        return data

    @classmethod
    def get_by_id(cls, _id):
        data = cls.query.get(_id)
        return data

    @classmethod
    def add(cls, **kwargs):
        instance = cls(**kwargs)
        db.session.add(instance)
        cls.commit_changes()
        return instance

    @classmethod
    def delete(cls, _id):
        cls.query.get(_id).delete()
        cls.commit_changes()

    @classmethod
    def update(cls, _id, **kwargs):
        instance = cls.query.get(_id)
        instance.update(**kwargs, synchronize_session=False)
        cls.commit_changes()
