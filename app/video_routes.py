from flask import request, jsonify
from flask_restful import Resource
from .video_controller import VideoController


class VideoRoute(Resource):
    def get(self):
        videos = VideoController.get_all()
        return jsonify([video.serialize() for video in videos])

    def post(self):
        content = request.get_json()
        VideoController.add(**content)
        return jsonify({})

    def put(self):
        pass

    def delete(self):
        pass
