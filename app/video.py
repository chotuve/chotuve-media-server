from .model import Model, db


class Video(Model):
    __tablename__ = 'videos'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20))
    description = db.Column(db.String(20))

    def __init__(self, name, description):
        self.name = name
        self.description = description

    def serialize(self):
        return {
            'name': self.name,
            'description': self.description
        }
