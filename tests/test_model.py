from base_test import BaseTest
from app import db
from app.video import Video


class ModelTest(BaseTest):

    def test_create_a_video_succesfully(self):
        user_data = {"name": "carlos", "description": "holaa"}
        video = Video.add(**user_data)

        assert video in db.session
